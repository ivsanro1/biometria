#!/usr/bin/env python -*- coding: utf-8 -*-

from __future__ import division # to avoid int divisions
import sys                      # to get script parameters
import matplotlib.pyplot as plt # to plot ROC curve
from itertools import product
import numpy as np              # statistical operations
import math                     # basically for sqrt
import argparse                 # for argument parsing capabilities
import cPickle                  # save k means classifier
import os
#from PIL import Image
from sklearn import decomposition
from sklearn.cluster import KMeans
from time import time
import scipy.ndimage


def load_data(path):
    """
    Returns in an ndarray the values of each row of the data file. Each ndarray of the ndarray is an image
    """
    print("Loading data from %s..." % (path))
    t0 = time()

    data = [] # later it will be converted to np array
    with open(path) as f:
        for line in f:
            x = np.fromstring(line, dtype=float, sep=' ') # one image
            data += [x]

    print("... done in %0.3fs" % (time() - t0))


    return np.array(data)

def concatenate(a,b):
    """
    Concatenates 2 numpy arrays. Used to concatenate faces and no faces for PCA
    """
    print("Concatenating array 1 with shape %s with array 2 with shape %s..." % (a.shape, b.shape))
    t0 = time()

    ret = np.concatenate((data_faces, data_nofaces), axis=0)

    print("... resulting array of shape %s. Done in %0.3fs" % (ret.shape, time() - t0))

    return ret


def crop_center(img,cropx,cropy):
    y,x = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    return img[starty:starty+cropy,startx:startx+cropx]


def split_matrix(matrix, parts_shape=(6,6), results_as_vectors=True):
    """ Gets a nxm matrix and splits it into parts of (x,y) elements. Default: Splits of 6x6
    If 'results_as_vectors' is true, the returned splits will be in vector form
    """

    if(matrix.shape[0] % parts_shape[0] != 0):
        raise ValueError('Matrix of %i rows cannot be split into submatrices of %i rows.' % (matrix.shape[0], parts_shape[0]) )

    if(matrix.shape[1] % parts_shape[1] != 0):
        raise ValueError('Matrix of %i columns cannot be split into submatrices of %i columns.' % (matrix.shape[1], parts_shape[1]) )

    splits = []

    h_splits = matrix.shape[0] // parts_shape[0] # Number of row splits
    v_splits = matrix.shape[1] // parts_shape[1] # Number of col splits

    for row in range(h_splits): # 0, 1, 2, 3
        for col in range(v_splits): # 0, 1, 2, 3
            split = matrix[row*parts_shape[0]:(row+1)*parts_shape[0],  # 0:5, 6:11, etc.
						   col*parts_shape[1]:(col+1)*parts_shape[1]]

            splits += [split]

    if(results_as_vectors):
        splits = [split.reshape(-1) for split in splits] # .reshape(-1) converts a matrix into a vector

    return splits



def parse_args():
    parser = argparse.ArgumentParser(description='placeholder.')


    parser.add_argument('--load', action='store_true', help='loads PCA and KMeans instead of calculating them. Files must be under the "objects" directory, named as pca.pkl and kmeans.pkl respectively.')


    return parser.parse_args()


if __name__ == "__main__":

    args = parse_args()

    image_shape = (24, 24)
    subregions_shape = (6, 6)

    nr = int(image_shape[0]/subregions_shape[0] * image_shape[1]/subregions_shape[1])

    path_faces = 'data/faces/dfFaces_24x24_norm'
    path_nofaces = 'data/nofaces/NotFaces_24x24_norm'

    savefile_pca = 'objects/'+'pca.pkl'
    savefile_kmeans = 'objects/'+'kmeans.pkl'
    if not os.path.exists('objects'):
        os.makedirs('objects')

    data_faces = load_data(path_faces)
    data_nofaces = load_data(path_nofaces)


    faces_nofaces = concatenate(data_faces, data_nofaces)

    data_subregions = []

    print("Getting subregions of all images...")
    t0 = time()

    # For every image (faces and nofaces)
    for img_vector in faces_nofaces:
        img_matrix = np.reshape(img_vector, image_shape) # reshape vector into 24x24 matrix
        subregions = split_matrix(img_matrix)            # get 16 subregions in a list


        data_subregions += subregions # accumulate subregions

    data_subregions = np.array(data_subregions)

    print("... done in %0.3f" % (time() - t0))


    #data_subregions = np.array(data_subregions) # convert to np array

    n_components = 10

    if(args.load):
        print("Loading PCA from %s..." % savefile_pca)
        t0 = time()
        with open(savefile_pca, 'rb') as fid:
            pca = cPickle.load(fid)
        print("... done in %0.3f" % (time() - t0))

    else:
        print("Calculating PCA of all the subregions...")
        t0 = time()
        pca = decomposition.PCA(n_components=n_components)
        pca.fit(data_subregions)
        print("... done in %0.3f" % (time() - t0))

        print("Saving PCA data into %s ..." % (savefile_pca))
        with open(savefile_pca, 'wb') as fid:
            cPickle.dump(pca, fid)





    print("Projecting subregions of dimensionality d = %i to a subspace of dimensionality d = %i ..." % (data_subregions.shape[1], n_components))
    t0 = time()
    projected_data_subregions = pca.transform(data_subregions)
    print("... done in %0.3f" % (time() - t0))


    n_clusters = 64

    if(args.load):
        print("Loading KMeans from %s..." % savefile_kmeans)
        t0 = time()
        with open(savefile_kmeans, 'rb') as fid:
            kmeans = cPickle.load(fid)
        print("... done in %0.3f" % (time() - t0))

    else:
        print("Calculating KMeans of projected data subregions...")
        t0 = time()
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(projected_data_subregions)
        print("... done in %0.3f" % (time() - t0))

        print("Saving K-Means classifier data into %s ..." % (savefile_kmeans))
        with open(savefile_kmeans, 'wb') as fid:
            cPickle.dump(kmeans, fid)




    # Train tables. Ppqf = P(pos_i | q_i, face)   ...   Pqf = P(q_i | face) ...
    #               Ppqn = P(pos_i | q_i, noface) ...   Pqn = P(q_i | noface)

    # Pqf = P(q_i | face)
    # Ppqf = P(pos_i | q_i, face)



    Pqf = np.zeros(n_clusters)
    Ppqf = np.zeros((n_clusters, nr)) # 32 x 16

    print("Calculating tables of probability distributions for faces...")
    t0 = time()

    for face in data_faces: # for each face
        face_matrix = np.reshape(face, image_shape) # reshape vector into 24x24 matrix
        subregions = split_matrix(face_matrix) # get the 16 subregions as vectors
        projected_subregions = pca.transform(subregions) # project the subregions
        qs = kmeans.predict(projected_subregions) # calculate the corresponding mean

        for i in range(len(qs)): # for all subregions in the image
            Pqf[qs[i]] += 1 # qs[i] is the predicted kmean of the subregion. Sum 1 to the count
            Ppqf[qs[i]][i] += 1 # given the quantum (mean), add one to the count of that quantum in the position i (subregion i)


    # normalize Pqf - Make it a probability distribution
    z = len(data_faces)*nr
    Pqf = [count/z for count in Pqf]

    # smooth Ppqf. Add 1 to any 0 value
    for i in xrange(Ppqf.shape[0]):
        for j in xrange(Ppqf.shape[1]):
            if(Ppqf[i,j] == 0):
                Ppqf[i,j] = 1



    # normalize Ppqf for each row
    for row in xrange(Ppqf.shape[0]):
        Ppqf[row] /= np.sum(Ppqf[row]) # here, we don't know beforehand how many times we have summed in one row (i.e. we saw a specific quantum)

    print("... done in %0.3f" % (time() - t0))


    # Pqn = P(q_i | noface)
    # Ppqn = P(pos_i | q_i, noface)

    Pqn = np.zeros(n_clusters)
    # Ppqn = 1.0/nr

    print("Calculating table of probability distribution for no-faces...")
    t0 = time()

    for noface in data_nofaces:
        noface_matrix = np.reshape(noface, image_shape)
        subregions = split_matrix(noface_matrix)
        projected_subregions = pca.transform(subregions)
        qs = kmeans.predict(projected_subregions)

        for i in range(len(qs)): # for all those subregions in the image
            Pqn[qs[i]] += 1

    # normalize Pqn - Make it a probability distribution
    z = len(data_nofaces)*nr
    Pqn = [count/z for count in Pqn]

    print("... done in %0.3f" % (time() - t0))

    print("Saving tables in directory 'objects'...")
    with open('objects/Pqf.pkl', 'wb') as fid:
        cPickle.dump(Pqf, fid)
    with open('objects/Ppqf.pkl', 'wb') as fid:
        cPickle.dump(Ppqf, fid)
    with open('objects/Pqn.pkl', 'wb') as fid:
        cPickle.dump(Pqn, fid)

    print("... tables successfully saved.")

    #components_ = pca.components_

    #first_component = components_[0]
    #first_component = np.reshape(first_component, (6,6))



    #plt.imshow(first_component, cmap='gray')

    #print image
    #print (split_matrix(image, copy=True)[0].shape)



    #split_matrix(image, (4,5))

    #image = scipy.ndimage.interpolation.rotate(image, 10.0)
    #image = crop_center(image, 24, 24)
    #plt.imshow(image, cmap='gray')
    #plt.show()


    #print(first_component.shape)
