#!/usr/bin/env python -*- coding: utf-8 -*-

from __future__ import division # to avoid int divisions
import sys                      # to get script parameters
import matplotlib.pyplot as plt # to plot ROC curve
from itertools import product
import numpy as np              # statistical operations
import math                     # basically for sqrt
import argparse                 # for argument parsing capabilities
import cPickle                  # save k means classifier
import os
#from PIL import Image
from sklearn import decomposition
from sklearn.cluster import KMeans
from time import time
import scipy.ndimage
from skimage.transform import pyramid_gaussian
from PIL import Image
import matplotlib.patches as patches

def load_data(path):
    """
    Returns in an ndarray the values of each row of the data file. Each ndarray of the ndarray is an image
    """
    print("Loading data from %s..." % (path))
    t0 = time()

    data = [] # later it will be converted to np array
    with open(path) as f:
        for line in f:
            x = np.fromstring(line, dtype=float, sep=' ') # one image
            data += [x]

    print("... done in %0.3fs" % (time() - t0))


    return np.array(data)

def concatenate(a,b):
    """
    Concatenates 2 numpy arrays. Used to concatenate faces and no faces for PCA
    """
    print("Concatenating array 1 with shape %s with array 2 with shape %s..." % (a.shape, b.shape))
    t0 = time()

    ret = np.concatenate((data_faces, data_nofaces), axis=0)

    print("... resulting array of shape %s. Done in %0.3fs" % (ret.shape, time() - t0))

    return ret


def crop_center(img,cropx,cropy):
    y,x = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    return img[starty:starty+cropy,startx:startx+cropx]


def split_matrix(matrix, parts_shape=(6,6), results_as_vectors=True):
    """ Gets a nxm matrix and splits it into parts of (x,y) elements. Default: Splits of 6x6
    If 'results_as_vectors' is true, the returned splits will be in vector form
    """

    if(matrix.shape[0] % parts_shape[0] != 0):
        raise ValueError('Matrix of %i rows cannot be split into submatrices of %i rows.' % (matrix.shape[0], parts_shape[0]) )

    if(matrix.shape[1] % parts_shape[1] != 0):
        raise ValueError('Matrix of %i columns cannot be split into submatrices of %i columns.' % (matrix.shape[1], parts_shape[1]) )

    splits = []

    h_splits = matrix.shape[0] // parts_shape[0] # Number of row splits
    v_splits = matrix.shape[1] // parts_shape[1] # Number of col splits

    for row in range(h_splits): # 0, 1, 2, 3
        for col in range(v_splits): # 0, 1, 2, 3
            split = matrix[row*parts_shape[0]:(row+1)*parts_shape[0],  # 0:5, 6:11, etc.
						   col*parts_shape[1]:(col+1)*parts_shape[1]]

            splits += [split]

    if(results_as_vectors):
        splits = [split.reshape(-1) for split in splits] # .reshape(-1) converts a matrix into a vector

    return splits


def classify(region_as_vector, l):
    """
    Parameters:
    1. region: the image region, represented as a vector.
    2. l: lambda. The value to decide if the region is a face or not

    Returns:
    - True if it determines that the region is a face
    - False if not
    """
    t0 = time()
    region_as_matrix = np.reshape(region_as_vector, image_shape) # reshape vector to matrix
    subregions = split_matrix(region_as_matrix)            # get 16 subregions in a list
    projected_subregions = pca.transform(subregions)
    qs = kmeans.predict(projected_subregions)

    acum = 1.
    for i in xrange(nr):
        if  ((Ppqf[qs[i]][i] * Pqf[qs[i]]) / (Pqn[qs[i]]/nr) == 0):
            acum *= 0.01
        else:
            acum *= (Ppqf[qs[i]][i] * Pqf[qs[i]]) / (Pqn[qs[i]]/nr)
        #print acum

    #print("...classif done in %0.3f" % (time() - t0))
    if(acum > l):
        return (True, acum)
    else:
        return (False, acum)


def sliding_window(image, stepSize, windowSize):
	# slide a window across the image
	for y in xrange(0, image.shape[0], stepSize):
		for x in xrange(0, image.shape[1], stepSize):
			# yield the current window
			yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])


def parse_args():
    parser = argparse.ArgumentParser(description='placeholder.')


    parser.add_argument('--load', action='store_true', help='loads PCA and KMeans instead of calculating them. Files must be under the "objects" directory, named as pca.pkl and kmeans.pkl respectively.')


    return parser.parse_args()


if __name__ == "__main__":

    global Pqf
    global Ppqf
    global Pqn
    global Ppqn
    global image_shape
    global subregions_shape
    global nr
    global pca
    global kmeans
    l = 60000000
    downscale = 1.05


    image_shape = (24, 24)
    subregions_shape = (6, 6)
    nr = int(image_shape[0]/subregions_shape[0] * image_shape[1]/subregions_shape[1])

    args = parse_args()

    path_faces = 'data/faces/dfFaces_24x24_norm'
    path_nofaces = 'data/nofaces/NotFaces_24x24_norm'

    savefile_pca = 'objects/'+'pca.pkl'
    savefile_kmeans = 'objects/'+'kmeans.pkl'

    data_faces = load_data(path_faces)
    data_nofaces = load_data(path_nofaces)

    # load tables
    print("Loading tables, PCA and Kmeans classifier data...")

    with open('objects/Pqf.pkl', 'rb') as fid:
        Pqf = cPickle.load(fid)
    with open('objects/Ppqf.pkl', 'rb') as fid:
        Ppqf = cPickle.load(fid)
    with open('objects/Pqn.pkl', 'rb') as fid:
        Pqn = cPickle.load(fid)

    Ppqn = 1./nr



    # load PCA
    with open(savefile_pca, 'rb') as fid:
        pca = cPickle.load(fid)

    # load Kmeans
    with open(savefile_kmeans, 'rb') as fid:
        kmeans = cPickle.load(fid)

    print("... successfully loaded.")

    img = Image.open('noface2.jpg').convert('LA')
    width, height = img.size


    img = [x[0] for x in list(img.getdata())]

    mu = np.mean(img)
    sigma = np.std(img)

    img = [(x-mu)/sigma for x in img]


     #img = np.reshape(img, (height,width))

    index = 345

    img = np.reshape(data_faces[index], (24,24))

    sumeans = 0.

    for face in data_faces:
        sumeans += np.mean(face)


    print sumeans/len(data_faces)

    print ("MEAN = %.3f " % sumeans/int(len(data_faces)))

    #print data_faces[index]


    # Create figure and axes
    fig,ax = plt.subplots(1)





    # Display the image
    ax.imshow(img, cmap='gray')

    print (classify(img,l))


    plt.show()
