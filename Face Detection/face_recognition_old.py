
from __future__ import print_function

from time import time
import logging
import matplotlib.pyplot as plt

import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn import datasets
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn import decomposition
from sklearn.svm import SVC


np.random.seed(5)

centers = [[1, 1], [-1, -1], [1, -1]]
iris = datasets.load_iris()
X = iris.data
y = iris.target

print(X)

fig = plt.figure(1, figsize=(4, 3))
plt.clf()


plt.cla()
pca = decomposition.PCA(n_components=3)
pca.fit(X)
X = pca.transform(X)
