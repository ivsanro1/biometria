#!/usr/bin/env python -*- coding: utf-8 -*-

from __future__ import division # to avoid int divisions
import sys                      # to get script parameters
import matplotlib.pyplot as plt # to plot ROC curve
from itertools import product
import numpy as np              # statistical operations
import math                     # basically for sqrt
import argparse                 # for argument parsing capabilities

def restricted_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x

def get_scores_from_file(path_client_scores, path_impostor_scores):
    """
    --- Parameter(s): 1.- Relative path of the client scores.
                      2.- Relative path of the impostor scores.

    --- Return: In a tuple, the clients' and impostor's scores, each as an array of values.
    (clients_values, impostor_values)
    """

    with open(path_client_scores) as f:
        client_scores = [float(score.split()[-1]) for score in f.readlines()]

    with open(path_impostor_scores) as f:
        impostor_scores = [float(score.split()[-1]) for score in f.readlines()]

    return (sorted(client_scores), sorted(impostor_scores))



def h(x):
    if(x > 0):
        return 1
    elif(x == 0):
        return 0.5
    else:
        return 0




def get_ROC_from_scores(client_scores, impostor_scores):
    """
    --- Parameter(s): 1.- Sorted client scores.
                      2.- Sorted impostor scores.

    --- Return: In a tuple:
                      1.- An array of tuples, where each tuple are the (x,y) values (points) of the ROC curve.
                      2.- The area under the ROC curve.

                      ([(x1, y1), ..., (xn, yn)], area)

    Notes:
        * //The next assumption is made: a score must be strictly higher than the threshold in order to be accepted.
    """

    pointer_c = 0
    pointer_i = 0
    thr = 0
    num_c = len(client_scores) # Number of scores, not the number of clients. Used to calculate the ratios
    num_i = len(impostor_scores)
    ROC_data = []

    # Starting case, when thr=0
    while(client_scores[pointer_c] < thr):
        pointer_c +=1

    while(impostor_scores[pointer_i] < thr):
        pointer_i +=1

    FP = num_i - pointer_i  # From pointer location to the end of the score list is the number of impostors in (FP).
    FN = pointer_c # From the beginning of the score list to the pointer is the number of clients out (FN).


    FNR = FN/num_c
    FPR = FP/num_i



    ROC_data += [(FPR, 1-FNR)] # x = FP, y = 1-FN



    # General case, when thr is in ]0, 1]

    while(pointer_i < num_i - 1 or pointer_c < num_c - 1):
        if(client_scores[pointer_c] < impostor_scores[pointer_i]): # The next lower score between the C and I scores is the C score (hence, we get one more FN). Calculate next ROC point depending on the new FN
            if(pointer_c < num_c - 1):
                pointer_c += 1

                FN += 1
                FNR = FN/num_c
                ROC_data += [(FPR, 1-FNR)]

            else:
                pointer_i += 1

                FP -= 1
                FPR = FP/num_i
                ROC_data += [(FPR, 1-FNR)]

        else: # The next lower score between the C and I scores is the I score (hence, we get one less FP). Calculate next ROC point depending on the new FP
            if(pointer_i < num_i - 1):
                pointer_i += 1

                FP -= 1
                FPR = FP/num_i
                ROC_data += [(FPR, 1-FNR)]

            else:
                pointer_c += 1

                FN += 1
                FNR = FN/num_c
                ROC_data += [(FPR, 1-FNR)]



    # Area under ROC curve. Reference: MANN-WHITNEY TEST

    acum = 0

    for i in range(len(client_scores)):
        for j in range(len(impostor_scores)):
            acum += h(client_scores[i] - impostor_scores[j])

    AROC = acum/(num_c*num_i)


    return (ROC_data, AROC)



def plot_from_ROC_data(ROC_data, ROC_data2 = None):
    fig = plt.figure()
    fig.canvas.set_window_title('ROC Curve(s)') 
    plt.plot([point[0] for point in ROC_data], [point[1] for point in ROC_data])
    if(ROC_data2 != None):
        plt.plot([point[0] for point in ROC_data2], [point[1] for point in ROC_data2])
        plt.legend(['Original system', 'Compared system'], loc='lower right')
    plt.axis([0, 1, 0, 1])
    plt.ylabel('1-FN')
    plt.xlabel('FP')
    plt.show()


def get_FPR(FNR, ROC_data, client_scores):
    """
    --- Parameter(s): 1.- Desired fixed ratio of False Negatives.
                      2.- The data containing the ROC curve points.
                      3.- Sorted client scores.

    --- Return: In a tuple: (FPR, thr), where:
                      1.- FPR is the best-case ratio of False Positives given the ratio of False Negatives.
                      2.- thr is the threshold needed to achieve that case.
    """

    # --- GET FPR ---

    # Use 1-FNR, as the points' 'y' value take that format
    FNR = 1-FNR
    # Get index of the FNR closest to the FNR given as parameter
    val = min([point[1] for point in ROC_data], key=lambda x:abs(x-FNR)) # Closest existing FNR to the FNR specified
    i = [point[1] for point in ROC_data].index(val) # Index of that existing FNR

    # Get the related FPR associated with that FNR index
    FPR = [point[0] for point in ROC_data][i]

    # --- GET THR ---

    # Given the FNR, we take out the FNR portion of the clients and get the one that makes the threshold

    index = int(len(client_scores)*(1-FNR))
    thr = client_scores[index]

    return (FPR, thr)




def get_FNR(FPR, ROC_data, impostor_scores):
    """
    --- Parameter(s): 1.- Desired fixed ratio of False Positives.
                      2.- The data containing the ROC curve points
                      3.- Sorted impostor scores.

    --- Return: In a tuple: (FNR, thr), where:
                      1.- FNR is the best-case ratio of False Negatives given the ratio of False Positives.
                      2.- thr is the threshold needed to achieve that case.
    """

    # --- GET FNR ---

    val = min([point[0] for point in ROC_data], key=lambda x:abs(x-FPR))
    i = [point[0] for point in ROC_data].index(val)

    # Get the FNR associated with that index
    FNR = [point[1] for point in ROC_data][i]

    # We have 1-FNR, the user wants to see FNR
    FNRold = FNR
    FNR = 1 - FNR

    # --- GET THR ---

    # Given the FPR, we take out the FPR portion of the impostors and get the one that makes the threshold

    index = int(len(impostor_scores)*(1-FPR))
    thr = impostor_scores[index]

    return (FNR, thr)


def get_F(THR, client_scores, impostor_scores):
    """
    --- Parameter(s): 1.- Fixed threshold
                      2.- Sorted array of client scores.
                      3.- Sorted array of impostor scores.

    --- Return: In a 4-tuple: (FN, FNR, FP, FPR), where:
                      1.- FN is the amount of False Negatives
                      2.- FNR is the False Negative Ratio
                      3.- FP is the amount of False Positives
                      4.- FPR is the False Positive Ratio
    """

    FN = 0.0
    FP = 0.0

    for i in range(len(client_scores)):
        if(client_scores[i] < THR):
            FN += 1.0
        else:
            break

    for i in reversed(range(len(impostor_scores))):
        if(impostor_scores[i] >= THR):
            FP += 1.0
        else:
            break

    FNR = FN/len(client_scores)
    FPR = FP/len(impostor_scores)

    return (FN, FNR, FP, FPR)


def get_FPReqFNR(ROC_data, epsilon, client_scores, impostor_scores):
    """
    --- Parameter(s): 1.- The data containing the ROC curve points.
                      2.- The amount of equality desired between FPR and FNR. More epsilon brings more space.

    --- Return: In an array: [FPR, FNR, thr], where:
                      1.- Ratio of False Positives and ...
                      2.- ... ratio of False Negatives that are closest to each other.
                      3.- thr is the threshold needed to achieve that case.
    """

    results = []
    for i in range(len(ROC_data)):
        FNR = 1 - ROC_data[i][1]
        if(abs(ROC_data[i][0] - FNR) < epsilon):
            results += (ROC_data[i][0], FNR)
            break

    FPR = results[0]
    FNR = results[1]

    # from the FN:
    index = int(len(client_scores)*FNR)
    thrFN = client_scores[index]

    # from the FP (for completition and correction purposes, it is not used)
    index = int(len(impostor_scores)*(1-FPR))
    thrFP = impostor_scores[index]

    results += [thrFN]

    return results


def get_dprime(client_scores, impostor_scores):
    return (np.mean(client_scores) - np.mean(impostor_scores))/(math.sqrt(np.var(client_scores)-np.var(impostor_scores)))

def parse_args():
    parser = argparse.ArgumentParser(description='Calculates the ROC curve, along with other optional operations wrt the ROC curve.')
    parser.add_argument('client_scores_path', metavar='client_scores',
                        help='relative path to positive (i.e. client) scores.')
    parser.add_argument('impostor_scores_path', metavar='impostor_scores',
                        help='relative path to negative (i.e. impostor) scores.')

    parser.add_argument('-fn', metavar='x', nargs=1, type=restricted_float,
                        help='sets a fixed value of FN and calculates the necessary threshold and the corresponding FP')
    parser.add_argument('-fp', metavar='x', nargs=1, type=restricted_float,
                        help='sets a fixed value of FP and calculates the necessary threshold and the corresponding FN')

    parser.add_argument('-thr', metavar='x', nargs=1, type=restricted_float,
                        help='sets a fixed value of the threshold and calculates the amount of FP, FN, and the respective ratios.')

    parser.add_argument('-compare', nargs=2, metavar=('client_scores_B','impostor_scores_B'),
                        help='compares both systems in the graph (if --graph is used)')

    parser.add_argument('--graph', action='store_true', help='shows the graph of the ROC curve')


    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()


    scores = get_scores_from_file(args.client_scores_path, args.impostor_scores_path)
    ROC_data, AreaROC = get_ROC_from_scores(scores[0], scores[1])
    dprime = get_dprime(scores[0], scores[1])
    FPReqFNR = get_FPReqFNR(ROC_data, 0.001, scores[0], scores[1])

    print ('\nIdentification scores analysis: \n')
    print ('Area under ROC curve: ' + str(round(AreaROC,4)))
    print ('d-prime: ' + str(round(dprime,4)))
    print ('FPR equals FNR when both are: ' + str(round(FPReqFNR[0],3)))
    print ('FPR equals FNR when the threshold is: ' + str(round(FPReqFNR[2],4)))




    if(args.graph):
        if(args.compare != None):
            scores_2 = get_scores_from_file(args.compare[0], args.compare[1])
            ROC_data_2, AreaROC_2 = get_ROC_from_scores(scores_2[0], scores_2[1])
            plot_from_ROC_data(ROC_data, ROC_data_2)
        else:
            plot_from_ROC_data(ROC_data)



    if(args.thr != None):
        fixed_THR_results = get_F(args.thr[0], scores[0], scores[1])
        print ('\nSetting the threshold (THR) to ' + str(args.thr[0]) + ' gives the next results:')
        print ('Amount of false negatives: ' + str(int(fixed_THR_results[0])) + ' out of ' + str(len(scores[0])) + ' scores. False Negative Ratio: ' + str(round(fixed_THR_results[1], 4)))
        print ('Amount of false positives: ' + str(int(fixed_THR_results[2])) + ' out of ' + str(len(scores[1])) + ' scores. False Positive Ratio: ' + str(round(fixed_THR_results[3], 4)))

    if(args.fp != None):

        fixed_FNR_results = get_FNR(args.fp[0], ROC_data, scores[1])

        print ('\nSetting the FP ratio to ' + str(args.fp[0]) + ' gives the next results:')
        print ('The best FN ratio for that FP ratio is: ' + str(round(fixed_FNR_results[0], 4)))
        print ('The threshold needed to achieve this case is: ' + str(round(fixed_FNR_results[1], 4)))

    if(args.fn != None):

        fixed_FPR_results = get_FPR(args.fn[0], ROC_data, scores[0])

        print ('\nSetting the FN ratio to ' + str(args.fn[0]) + ' gives the next results:')
        print ('The best FP ratio for that FN ratio is: ' + str(round(fixed_FPR_results[0], 4)))
        print ('The threshold needed to achieve this case is: ' + str(round(fixed_FPR_results[1], 4)))
