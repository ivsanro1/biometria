#!/usr/bin/env python -*- coding: utf-8 -*-

from __future__ import division # to avoid int divisions
import sys 						# to get script parameters
import matplotlib.pyplot as plt # to plot ROC curve
from itertools import product
import numpy as np              # statistical operations
import math 					# basically for sqrt


def restricted_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x



import argparse

parser = argparse.ArgumentParser(description='Calculates the ROC curve, along with other optional operations wrt the ROC curve.')
parser.add_argument('client_scores_path', metavar='client_scores',
                    help='relative path to positive (i.e. client) scores.')
parser.add_argument('impostor_scores_path', metavar='impostor_scores',
                    help='relative path to negative (i.e. impostor) scores.')

parser.add_argument('-fn', metavar='x', nargs=1, type=restricted_float,
					help='sets a fixed value of FN and calculates the necessary threshold and the corresponding FP')
parser.add_argument('-fp', metavar='x', nargs=1, type=restricted_float,
					help='sets a fixed value of FP and calculates the necessary threshold and the corresponding FN')

parser.add_argument('--graph', action='store_true',
					help='shows the graph of the ROC curve')

parser.add_argument('-compare', nargs=2, metavar=('client_scores_B','impostor_scores_B'),
                    help='compares both systems in the graph (if --graph is used)')

args = parser.parse_args()
print args
